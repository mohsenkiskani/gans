This is my implementation of some gans. In the implementation of original GAN on MNIST, I faced the following some problems and I have some observations. 

1 - Originally, I was not sure how to implement the loss function and how to run the backpropagation algorithm. I implemented my own loss function based on the original GAN paper. Then I was sampling from both noise and data and concatenating them together and training the discriminator based on that and then training the generator. I was not "detaching" the generator and so the generator network was being updated twice in each iteration. I was not sure whether my loss function is correct or not. However, with this implementation, my model went to "mode collapse". Then I used a loss function similar to the ones in [1] and [2]. I also used an architecture similar to the one in [2]. I was not using any transforms on my data originally and I also started to transform the images similar to [2]. I also read [5] and [6] on mode collapse.

2 - I was using SGD with momentum 0.9 and I was going to "mode collapse" all the time. I was not sure how to avoid mode collapse. I reviewed a bunch of blogs including [3] and [4] and I could not get my code to work. Then I switched to Adam similar to [2] and suddentl the code started to work. So I guess the choice of optimization algorithms is also important. I was able to get reasonable results after around 200 training epochs. I think the results would still improve if I train for more than 200 epochs. 

3 - As explained earlier, many of the improvements that helped me in getting the model to work were based on the implementation in [2] which is using many ideas from the DCGAN paper in [7]. For instance, it is in the DCGAN paper that is suggested to use a Tanh activation for the output of the generator or to use Adam optimization with parameters 0.5 and 0.999 or LeakyReLU with negative slope of 0.2 or the learning rate of 0.0002. Interesting to note that these parameters are kind of very optimal and carefully chosen. For instance, when I changed the learning rate from 0.0002 to 0.0001 in the beginning iterations, the generated images were blurry and I waited to get better resolution but after around 500 iterations, the model came to a partial mode collapse which appeared to deteriorate with training so I stopped it. Also, when I change the learning rate to 0.0001 the partial mode collapse still exists but it is less visible. I noticed the problem of partial model collapse when I ran the code in [2]. I was not able to get a good sample that contains all the digits with uniform probabilities. I think this is a serious problem in GANs. 

[1] https://github.com/devnag/pytorch-generative-adversarial-networks/blob/master/gan_pytorch.py

[2] https://github.com/eriklindernoren/PyTorch-GAN/blob/master/implementations/gan/gan.py 

[3] https://medium.com/@utk.is.here/keep-calm-and-train-a-gan-pitfalls-and-tips-on-training-generative-adversarial-networks-edd529764aa9 

[4] https://medium.com/@jonathan_hui/gan-why-it-is-so-hard-to-train-generative-advisory-networks-819a86b3750b 

[5] http://aiden.nibali.org/blog/2017-01-18-mode-collapse-gans/

[6] https://medium.com/@jonathan_hui/gan-unrolled-gan-how-to-reduce-mode-collapse-af5f2f7b51cd

[7] https://arxiv.org/pdf/1511.06434.pdf
