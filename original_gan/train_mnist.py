import torch
from models import *
import torchvision.datasets as datasets
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import torch.optim as optim
import argparse

parser = argparse.ArgumentParser(description='GAN MNIST Example')
parser.add_argument(
        '-b',
        '--batch_size',
        type=int,
        default=512,
        help="""Batch size""")

parser.add_argument(
        '-i',
        '--iterations',
        type=int,
        default=3,
        help="""Total training iterations""")

parser.add_argument(
        '-lr',
        '--lr',
        type=float,
        default=0.001,
        help="""Learning rate""")

args = parser.parse_args()

batch_size = args.batch_size
iterations = args.iterations
learning_rate = args.lr

discriminator_update_steps_k = 1
#momentum = 0.9
log_interval = 10

gpu_id = torch.cuda.current_device()
cuda_name = torch.device(gpu_id)

mnist_trainset = datasets.MNIST(root='../data', train=True, download=True, transform=transforms.Compose(
            [transforms.Resize(28), transforms.ToTensor(), transforms.Normalize([0.5], [0.5])]))
train_dataloader = DataLoader(mnist_trainset, batch_size=batch_size, shuffle = True) 

generator = Generator()
discriminator = Discriminator()
#gen_optimizer = optim.SGD(generator.parameters(), lr=learning_rate, momentum=momentum)
#dis_optimizer = optim.SGD(discriminator.parameters(), lr=learning_rate, momentum=momentum)

gen_optimizer = optim.Adam(generator.parameters(), lr=learning_rate, betas=(0.5, 0.999))
dis_optimizer = optim.Adam(discriminator.parameters(), lr=learning_rate, betas=(0.5, 0.999))

train_losses = []
train_counter = []

criterion = torch.nn.BCELoss()

generator.cuda(cuda_name)
discriminator.cuda(cuda_name)
criterion.cuda(cuda_name)

generator.train()
discriminator.train()

for epoch in range(iterations):
    for k in range(discriminator_update_steps_k):
        for i_batch, sample_batched in enumerate(train_dataloader):
            discriminator.zero_grad()

            x_data_input = sample_batched[0].view(sample_batched[0].size(0), -1).cuda(cuda_name)
            real_data_probs = discriminator(x_data_input)
            data_targets = torch.ones_like(real_data_probs)    # Data labels are set to 1

            z_input = torch.randn([x_data_input.size(0), input_noise_dim]).cuda(cuda_name)

            disc_loss_real = criterion(real_data_probs, data_targets)
            disc_loss_real.backward()
            x_fake_input = generator(z_input).detach()    # detach to avoid training generator on these labels
            fake_probs = discriminator(x_fake_input)
            fake_targets = torch.zeros_like(fake_probs)    # Fake labels are set to 0
            disc_loss_fake = criterion(fake_probs, fake_targets)
            disc_loss_fake.backward()
            disc_loss = disc_loss_real + disc_loss_fake

            dis_optimizer.step()

            if i_batch % log_interval == 0:
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tDiscriminator\tLoss: {:.6f}\tReal precision: {:.4f}\tFake precision: {:.4f}'.format(epoch, i_batch * batch_size, len(train_dataloader) * batch_size, 100. * i_batch / len(train_dataloader), disc_loss.item(),\
                      torch.mean(real_data_probs).item(), torch.mean(fake_probs).item()))
                train_losses.append(disc_loss.item())
                train_counter.append(
                    (i_batch * batch_size) + ((epoch-1)*len(train_dataloader) * batch_size))
                torch.save(generator.state_dict(), "generator.pth")
                torch.save(discriminator.state_dict(), "discriminator.pth")


    for batch_idx, _ in enumerate(train_dataloader):
            generator.zero_grad()
            z_input = torch.randn([batch_size, input_noise_dim]).cuda(cuda_name)
            x_fake_input = generator(z_input)
            fake_output = discriminator(x_fake_input)

            fake_targets = torch.ones_like(fake_output)  # As opposed to the training of discriminator, the fake targets are set to 1.
            gen_loss = criterion(fake_output, fake_targets)
            gen_loss.backward()
            gen_optimizer.step()

            if batch_idx % log_interval == 0:
                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tGenerator\tLoss: {:.6f}\tFake precision: {:.4f}'.format(
                    epoch, batch_idx * batch_size, len(train_dataloader) * batch_size,
                    100. * batch_idx / len(train_dataloader), gen_loss.item(), torch.mean(fake_output).item()))
                train_losses.append(gen_loss.item())
                train_counter.append(
                    (i_batch * batch_size) + ((epoch-1)*len(train_dataloader) * batch_size))
                torch.save(generator.state_dict(), "generator.pth")
                torch.save(discriminator.state_dict(), "discriminator.pth")
